                </main><!-- end primary -->                                                                                   
            </section><!-- end content -->
           
			<footer><!-- begin footer -->
				<div id="footer"><!-- #footer -->            
					<div class="container"><!-- begin container -->
						<div class="row"><!-- begin row -->
							<div class="col-md-12"><!-- begin col -->
								<div class="footer-title">株式会社昭和商会</div>
								<div class="footer-info">
									<p>〒615-0015 京都府京都市右京区西院春日町２<br />TEL：075-312-7665 FAX：075-313-8854.<br />Copyright © 2015 株式会社昭和商会 All Rights Reserved.</p>
								</div>
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->        
				</div><!-- /#footer -->
			</footer><!-- end footer -->               
        </div><!-- end wrapper -->		
        <?php wp_footer(); ?>		
    </body>
</html>
