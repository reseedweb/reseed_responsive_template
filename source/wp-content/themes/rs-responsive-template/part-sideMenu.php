<div class="sideMenu">        
    <ul>
        <li data-height="59"><a href="<?php bloginfo('url'); ?>/#">SideMenu item 1</a></li>
        <li data-height="60"><a href="<?php bloginfo('url'); ?>/#">SideMenu item 2</a></li>
        <li data-height="60"><a href="<?php bloginfo('url'); ?>/#">SideMenu item 3</a></li>
        <li data-height="60"><a href="<?php bloginfo('url'); ?>/#">SideMenu item 4</a></li>
        <li data-height="57"><a href="<?php bloginfo('url'); ?>/#">SideMenu item 5</a></li>        
    </ul>
</div>
<script type="text/javascript">
	$(document).ready(function(){		
		$('.sideMenu ul li').each(function(){			
			if($(this).attr('data-height') != 'undefined'){
				$(this).children('a').css('line-height',parseInt($(this).attr('data-height')) + 'px');
			}
		});
	});
</script>