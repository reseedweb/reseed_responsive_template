<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <!--<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
		<title><?php if(is_home()){ echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?> </title>
		<!-- <title>?php wp_title(''); ?></title>	-->
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />        
        <!--<link rel="shortcut icon" href="" />-->
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '970px';
            var CONTENT_WIDTH = '950px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        		
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>		
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        				
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.waypoints.min.js" type="text/javascript"></script>
		<link href="<?php bloginfo('template_url'); ?>/css/slick.css" rel="stylesheet" />		
		<script src="<?php bloginfo('template_url'); ?>/js/slick.min.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.reseed.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jsdd.js" type="text/javascript"></script>
		<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>	
		<link href="<?php bloginfo('template_url'); ?>/css/jquery.sidr.light.css" rel="stylesheet" />		
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.sidr.min.js" type="text/javascript"></script>				
        <?php wp_head(); ?>		
    </head>
    <body>     
        <div id="screen_type"></div>		
        <div id="wrapper"><!-- begin wrapper -->
			<div id="top-header">
				<div id="top-header-info">
					<section id="top">
						<div class="container"><!-- begin container -->
							<div class="row clearfix"><!-- begin row -->
								<div class="col-md-12">
									<h1 class="top-text">関西・北陸・東海・中国・山陽での段ボール、プラダン、段ボールパレット・パッケージなら昭和商会</h1>							
								</div>						
							</div><!-- end row -->
						</div><!-- end container --> 										
					</section>
							
					<header class="slide"><!-- begin header -->
						<div id="header"><!-- begin header -->
							<div class="container"><!-- begin container -->
								<div id="headerContent" class="row clearfix"><!-- begin row -->							
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-5"><!-- begin col -->                        
										<?php if(is_front_page()) : ?>
											<h1 class="logo">
												<a href="<?php bloginfo('url'); ?>">
													<img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name'); ?>" />
												</a>
											</h1>
										<?php else : ?>
											<div class="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name'); ?>" /></a></div>
										<?php endif; ?>
									</div>									
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><!-- begin col -->                        										
										<div class="header-contact">
											<p class="headtxt">お気軽にお問い合わせください</p>
											<p class="headtel">TEL 075-312-7665</p>
										</div>										
										<a id="right-menu" href="#right-menu"><i class="fa fa-align-justify"></i></a>
										<a href="tel:0753127665"><i class="fa fa-phone-square"></i></a>
									</div><!-- end col --> 									
									<section id="top-mobile">																												
											<nav class="nav">
												<ul class="nav-list">
													<li class="nav-item"><a href="<?php bloginfo('url'); ?>/strength">会社の強み</a></li>
													<li class="nav-item nav-itemclr">
														<a class="nav-itemclr-a" href="<?php bloginfo('url'); ?>/#">商品紹介</a>
														<ul class="nav-submenu">
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/cardboard">段ボール</a></li>
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/pladan">プラダン</a></li>											
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/pallet">段ボールパレット</a></li>											
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/package">パッケージ</a></li>											
														</ul>
													</li>
													<li class="nav-item nav-itemclr">
														<a class="nav-itemclr-a"<?php bloginfo('url'); ?>/#">工場紹介</a>
														<ul class="nav-submenu">
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/kyoto">京都工場</a></li>
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/jyoyo">城陽工場</a></li>											
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/shiga">滋賀物流サービス</a></li>											
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/hukui">福井紙器彩感</a></li>											
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/okayama">岡山工場</a></li>											
															<li class="nav-submenu-item"><a href="<?php bloginfo('url'); ?>/mie">三重工場</a></li>											
														</ul>
													</li>
													<li class="nav-item"><a href="<?php bloginfo('url'); ?>/company">会社紹介</a></li>
													<li class="nav-item"><a href="<?php bloginfo('url'); ?>/contact">お問合せ</a></li>									
												</ul>
											</nav>															
									</section>
								</div><!-- end row -->
							</div><!-- end container -->        
						</div>
					</header><!-- end header -->
				</div>		           		
				<?php get_template_part('part','nav'); ?>							
			</div>			
			<?php if(is_home()) : ?>
                <?php get_template_part('part','slider'); ?>
            <?php endif; ?>		
			
			<?php if(!is_home()) : ?>
				<?php get_template_part('part','topmain'); ?>
			<?php endif; ?>
			
            <section id="content"><!-- begin content -->
                <main id="primary">