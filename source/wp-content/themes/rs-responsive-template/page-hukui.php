<?php get_header(); ?>
    <div class="primary-row container clearfix"><!-- begin container -->                    
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="workshop-main1">										
					<h3 class="workshop-main1-title">福井紙器彩感</h3>
					<div class="workshop-main1-text1">
						<p>FFG製造ライン(9尺)によって、フレキソ印刷から裁断、接合までを自動制御化。通常よりも大型の段ボール製品の製造が可能です。<br />また、木製パレットに代わり注目を集める、リサイクル率の高い段ボールパレットのご要望にも対応。<br />ロット、コスト面でもお客様に貢献しております。</p>
					</div>					
				</div>
            </div>			          
        </div>        
	</div><!-- end primary-row --> 	
   
	<div class="primary-row container clearfix"><!-- begin container -->                    
		<div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<h4 class="workshop-title1">街を歩けば</h4>
				<div class="workshop-text1">
					<p>福井県あわら市は、県の北端に位置する芦原町と金津町が合併してできた市で、福井平野から加越大地に広がっています。<br />北陸有数の温泉町として知られる芦原温泉や荒波が勇壮な海食景観を誇る東尋坊が有名で、当事業所のある金津町は古く江戸時代には、 金津宿と呼ばれ、福井藩の関所や奉行所が置かれるなど北陸街道の要所としても知られていました。</p>
				</div>
				<h5 class="workshop-title2">所在地</h5>
				<div class="workshop-text2">
					<p>〒919-0614<br />福井県あわら市伊井4郷之木1-1<br />TEL.0776-73-5250　FAX.0776-73-5252</p>
				</div>
				
				<h5 class="workshop-title2">敷地</h5>
				<div class="workshop-text2">
					<p>敷地面積：11387㎡<br />延床面積：3293㎡<br />平置倉庫：750㎡</p>
				</div>
				
				<h5 class="workshop-title2">製造業務内容</h5>
				<div class="workshop-text2">
					<p>FFG製造ライン(9尺)・平盤ダイカッター製造ライン<br />段ボールパレット製造<br />一般段ボール製品製造</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/hukui_content_top.jpg" alt="hukui" /></p>
			</div>
		</div> 		
	</div><!-- end primary-row --> 
	
	<div class="primary-row container clearfix"><!-- begin container -->                    
		<div class="row clearfix">
            <div class="col-md-12">
				<h3 class="workshop-slider-title">福井紙器彩感の写真紹介</h3>
				<?php get_template_part('part','factory');?>
			</div>						
		</div> 
	</div><!-- end primary-row --> 
	<div class="workshop-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d25759.47268654682!2d136.24438110000003!3d36.19248484999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z56aP5LqV55yM44GC44KP44KJ5biC5LyK5LqVNOmDt-S5i-acqDEtMQ!5e0!3m2!1svi!2s!4v1432635177575" width="100%" height="470" frameborder="0" style="border:0"></iframe>
	</div>	                                                          		                                                          	                                                                                                                                         	                          
<?php get_footer(); ?>