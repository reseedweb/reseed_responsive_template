/*----- js top-navi -----*/
$(window).on('load resize', function(){
	var w = $(window).width();
	var x = 500;
	var sp_x = 480;
	$("#content").waypoint({
		handler: function(direction) {
			if (direction == 'down' && w > x) {
				$("#top-header-info").slideUp();
			}else if(direction == 'up' && w > x){
				$("#top-header-info").slideDown();
			}
		}
	});						
});

$(document).ready(function() {
	/*----- js top-navi current -----*/
	$('a').each(function() {
		if ($(this).prop('href') == window.location.href) {
		  $(this).addClass('link-current');
		}
	});
	
	/*----- js sub-navi current -----*/
	$(".sub-navi ul li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("linksub-current");
        }
	});
	
	/*----- js top-main-slider -----*/
    $('.top-main-slider').slick({
		dots: false,
		infinite: true,
		slidesToScroll: 1,
		centerMode: true,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 2000,
		pauseOnHover:false,
		arrows:false
	});  
	
	/*----- js workshop -----*/
	$('.workshop-slider').slick({
		dots: true,
		infinite: true,
		arrows:false,
		speed: 300,
		autoplay: true,
		accessibility: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		centerMode: true,
		adaptiveHeight: true,
		responsive:[{
		breakpoint: 480,
		settings:{
		slidesToShow: 1,
		slidesToScroll: 1
		}
		}]
	});
	  
	/*----- js contact -----*/
	$('#zip').change(function(){					
		//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
		AjaxZip3.zip2addr(this,'','pref','addr');
	});
	
	/*----- js top-navi2 for sp -----*/
	$('#right-menu').sidr({
		name: 'sidr-right',
	    side: 'right',
		source : '#menu-toggle'
	});
	
	$(".sidr ul").on('click', 'li', function(){	
	if ($(this).children("ul").css('display') == 'none') {
		$(".sidr ul li ul").slideUp();
        $(this).children('ul').slideDown();
	}
	else{
	   $(this).children('ul').slideUp();
	}
    });
	
	/*----- js top-navi1 for sp -----*/
	// Append the mobile icon nav
	$('.nav').append($('<div class="nav-mobile"></div>'));
	// Add a <span> to every .nav-item that has a <ul> inside
	$('.nav-item').has('ul').prepend('<span class="nav-click"><i class="nav-arrow"></i></span>');
	// Click to reveal the nav
	$('.nav-mobile').click(function(){
		$('.nav-list').toggle( "slow" );
	});
	//.toggle( "slide" );   toggle( "fade" );
	// Dynamic binding to on 'click'
	$('.nav-list').on('click', '.nav-itemclr', function(){
		// Toggle the nested nav			
		if ($(this).children("ul").css('display') == 'none') {
			$(".nav .nav-list .nav-itemclr .nav-submenu").slideUp();
			$(this).children('ul').slideToggle('slow');	
		}
		else{
		   $(this).children('ul').slideUp();
		}
	});
	
	/*----- js effect change object -----*/
	/* Every time the window is scrolled ... */
	$(window).scroll( function(){						
		/* Check the location of each desired element */
		$('.wpb_animate_when_almost_visible').each( function(i){								
			var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			var bottom_of_window = $(window).scrollTop() + $(window).height();								
			/* If the object is completely visible in the window, fade it it */
			if( bottom_of_window > bottom_of_object ){           
				$(this).addClass('wpb_start_animation');
			}								
		}); 						
	});	
}); 