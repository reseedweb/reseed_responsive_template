var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
function is_mobile(){
    var screen_type = $('#screen_type').css('content');       
    if(
        screen_type === 'lg' || screen_type === 'md' || screen_type === 'sm' // chrome
        ||
        screen_type === '"lg"' || screen_type === '"md"' || screen_type === '"sm"' //firefox
        ||
        screen_type === undefined
    ) return false;
    return true;    
}
function get_screen_type(){
    var screen_type = $('#screen_type').css('content');    
    switch(screen_type){
        case undefined :        
            return 'ie';        
        case 'lg':
            return 'lg';        
        case '"md"':
        case 'md':
            return 'md';
        case 'sm':
        case '"sm"':
            return 'sm';
        case 'xs':
        case '"xs"':
            return 'xs';                     
        default:
            return 'lg';
    }
}
$(document).ready(function(){    
    $('#wrapper').addClass(get_screen_type());
    $( window ).resize(function(){
        $('#wrapper').attr('class', get_screen_type());
    });
});