<?php
/*
Plugin Name: bxslider
Plugin URI: http://wordpress.org/plugins/bx-slider/
Description: The slider plugin base on bx slider jquery
Author: Son Nguyen Minh
Version: 1.0
Author URI: http://congnghefibo.com/
*/

add_action('wp_footer','bx_slider_init');

function bx_slider_init()
{
	wp_enqueue_script( 'jquery-bx-slider', plugins_url( 'jquery.bxslider.min.js?v=3.2',__FILE__) );

	//wp_enqueue_style('nivos-slider-themes-default', plugins_url('themes/default/default.css', __FILE__ ) );
	//wp_enqueue_style('nivos-slider-themes-light', plugins_url('themes/light/light.css', __FILE__ ) );
	//wp_enqueue_style('nivos-slider-themes-dark', plugins_url('themes/dark/dark.css', __FILE__ ) );
	//wp_enqueue_style('nivos-slider-themes-bar', plugins_url('themes/bar/bar.css', __FILE__ ) );

	wp_enqueue_style('bx-slider', plugins_url('jquery.bxslider.css', __FILE__ ) );
}